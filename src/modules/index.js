import { Router } from "express";
import signup from "./services/auth"

const routes = new Router();

routes.use("/signup", signup);

export default routes;