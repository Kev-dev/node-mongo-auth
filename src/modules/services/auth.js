import AuthModel from "../auth/auth";

const signup = async (req, res) => {
  const { username, password, email } = req.body;
  try {
    if (!email || !password || !username) {
      throw new Error("All fields required");
    }
    const user = await AuthModel.create({ email, password, username });
    return res.status(200).json(user);
  } catch (error) {
    return res.status(400).json({ error: String(error) });
  }
};

export default signup;