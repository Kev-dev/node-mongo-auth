import express from "express";
import constants from "./config/constants";
import './config/db';
import middlewares from './config/middlewares';

import authRoutes from "./modules";

const app = express();

middlewares(app);
app.use("/auth", authRoutes);


app.listen(constants.PORT, () => {
  console.log(`App running on port ${constants.PORT}`);
  console.log(`App running in ${process.env.NODE_ENV}`);
});